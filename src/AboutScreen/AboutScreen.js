import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Linking } from 'react-native';

import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';

class AboutScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View styles={styles.container}>
        <Image
               source={require('../../assets/Images/front.png')}
               style={{width:responsiveWidth(100),
               height:responsiveHeight(40)}}/>
          <Text 
            style={{fontSize:responsiveFontSize(10),color:'#46748E',marginTop:responsiveHeight(1),
            marginLeft:responsiveWidth(25), fontWeight:'bold',fontStyle:'italic'}}>
              LCET
          </Text>
            <Text 
              style={{fontSize:responsiveFontSize(2),color:'#5594b7',
              justifyContent:'center',marginTop:responsiveHeight(1),alignItems:'center'}}>
                LCET is one of Foremost Centre for Higher Education in North India.
                We Emphasis on Overall Progress of Students empowering them to be Responsible Global Citizens.
                We Believe in Providing 21st Century World that is Practical, Joyful and Addresses the Need of Industry.
                Education at LCET is aimed at acquiring knowledge and skills, building character and improving employability of our young talent.
                LCET has been able to stand tall in the field of education in its journey since 2002, and has earned a pride of being one of the best.
                LCET provides better exposure to the students through continuous efforts on collaboration with leading industries and universities.
                High intellectual ability combined with strong academic background and capacity for hard work has made LCETIANS a preferred workforce all over the industry. 
            </Text>
          <TouchableOpacity 
          style={{marginLeft:responsiveWidth(35),marginTop:responsiveHeight(2),marginRight:responsiveWidth(37)}}
          onPress={() => Linking.openURL('https://lcetldh.com/')}>
            <Text style={{fontSize:responsiveFontSize(2),color:'white',backgroundColor:'#5594b7'}}>VIEW WEBSITE</Text>
          </TouchableOpacity>
              
      </View>
    );
  }
}

export default AboutScreen;
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
