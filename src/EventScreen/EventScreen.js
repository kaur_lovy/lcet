import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

class EventScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View styles={styles.container}>
        <Text> EventScreen </Text>
      </View>
    );
  }
}

export default EventScreen;
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
  
