import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

class AnnouncementScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View styles={styles.container}>
        <Text> AnnouncementScreen </Text>
      </View>
    );
  }
}

export default AnnouncementScreen;
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
