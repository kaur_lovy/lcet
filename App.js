import React,{Component} from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import {createAppContainer} from 'react-navigation';
import AppNavigator from '../LCET/router/MainRoute/MainRoute';

const AppIndex = createAppContainer(AppNavigator)  

export default class App extends Component{  
  render(){  
      return(  
          <View style={{flex:1}}>
             <StatusBar  
                backgroundColor='#4d8cb0'  
                barStyle='light-content' />
            <AppIndex/>  
           </View>  
      )  
  }  
}  
const styles = StyleSheet.create({  
  wrapper: {  
      flex: 1,  
  },  
  header:{  
      flexDirection: 'row',  
      alignItems: 'center',  
      justifyContent: 'space-between',  
      backgroundColor: 'red',  
      paddingHorizontal: 18,  
      paddingTop: 5,  
  } 
}) 
