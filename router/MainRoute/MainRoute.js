import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createAppContainer} from 'react-navigation';
import AboutScreen from '../../src/AboutScreen/AboutScreen'
import AnnouncementScreen from '../../src/AnnouncementScreen/AnnouncementScreen';
import Curriculumscreen from '../../src/CurriculumScreen/CurriculumScreen';
import EventScreen from '../../src/EventScreen/EventScreen';
import GalleryScreen from '../../src/GalleryScreen/GalleryScreen';
import SugComScreen from '../../src/SugComScreen/SugComScreen';

const AppNavigator = createMaterialTopTabNavigator(
    {
        About : AboutScreen,
        Announcement : AnnouncementScreen,
        Curriculum : Curriculumscreen,
        Event : EventScreen,
        Gallery : GalleryScreen,
        Sugggestions : SugComScreen
    },
    {   
            initialRouteName:"About",
            tabBarOptions: {
            scrollEnabled: true,  
            activeTintColor: 'white',  
            showIcon: false,  
            showLabel:true,  
            style: {  
                backgroundColor:'#4d8cb0'  
            }
        },
    }
)
export default createAppContainer(AppNavigator);